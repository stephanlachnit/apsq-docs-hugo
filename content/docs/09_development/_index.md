---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Module & Detector Development"
description: "How to develop new simulation modules and detector models."
weight: 9
---

This chapter provides a few brief recipes for developing new simulation modules and detector models for the Allpix Squared
framework. Before starting the development, the `CONTRIBUTING.md` file in the repository \[[@ap2-repo]\] should be consulted
for further information on the development process, code contributions and the preferred coding style for Allpix Squared.


[@ap2-repo]: https://gitlab.cern.ch/allpix-squared/allpix-squared
