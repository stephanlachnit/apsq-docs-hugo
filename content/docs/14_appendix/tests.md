---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "List of Tests"
---

## Framework Functionality Tests

Currently implemented framework functionality tests comprise:

-   `core/test_01-10_globalconfig_log_prng`:
    tests the possibility of logging individual random numbers used during the simulation
 -   `core/test_01-1_globalconfig_detectors`:
    test the framework behavior in case of a non-existent detector setup description file
 -   `core/test_01-2_globalconfig_modelpaths`:
    tests the correct parsing of additional model paths and the loading of the detector model.
 -   `core/test_01-3_globalconfig_log_format`:
    switches the logging format.
 -   `core/test_01-4_globalconfig_log_level`:
    sets a different logging verbosity level.
 -   `core/test_01-5_globalconfig_log_file`:
    configures the framework to write log messages into a file.
 -   `core/test_01-6_globalconfig_missing_model`:
    tests the behavior of the framework in case of a missing detector model file.
 -   `core/test_01-7_globalconfig_random_seed`:
    sets a defined random seed to start the simulation with.
 -   `core/test_01-8_globalconfig_random_seed_core`:
    sets a defined seed for the core component seed generator, e.g. used for misalignment.
 -   `core/test_01-9_globalconfig_librarydirectory`:
    tests the correct parsing and usage of additional library loading paths.
 -   `core/test_02-1_specialization_unique_name`:
    tests the framework behavior for an invalid module configuration: attempt to specialize a unique module for one detector instance.
 -   `core/test_02-2_specialization_unique_type`:
    tests the framework behavior for an invalid module configuration: attempt to specialize a unique module for one detector type.
 -   `core/test_02-3_specialization_name`:
    tests module instance specialization by name
 -   `core/test_02-4_specialization_type`:
    tests module instance specialization by type
 -   `core/test_03-10-geometry_unique_detectors`:
    tests if the case of multiple detectors with the same name is correctly caught
 -   `core/test_03-11-geometry_unique_passive`:
    tests if the case of multiple passive volumes with the same name is correctly caught
 -   `core/test_03-12_geometry_invalid_name`:
    tests if invalid detector names are correctly caught
 -   `core/test_03-1_geometry_g4_coordinate_system`:
    ensures that the Allpix Squared and Geant4 coordinate systems and transformations are identical.
 -   `core/test_03-2_geometry_rotations`:
    tests the correct interpretation of rotation angles in the detector setup file.
 -   `core/test_03-3_geometry_misaligned`:
    tests the correct calculation of misalignments from alignment precisions given in the detector setup file.
 -   `core/test_03-4_geometry_overwrite`:
    checks that detector model parameters are overwritten correctly
 -   `core/test_03-5_geometry_invalid_implant`:
    checks for correct detection of invalid implant size configurations
 -   `core/test_03-6_geometry_overlap`:
    checks for correct detection of volume overlaps in the geometry
 -   `core/test_03-7_geometry_wrapper`:
    checks for correct treatment of geometry wrappers and overlap calculations
 -   `core/test_03-8_geometry_noposition`:
    test the framework behavior with a detector with no position provided in the geometry
 -   `core/test_03-9_geometry_nodetector`:
    tests if missing detectors requested in individual module instances are correctly detected
 -   `core/test_04-10_configuration_invalid_combination`:
    tests if invalid configuration key combinations are correctly detected and reported
 -   `core/test_04-11_configuration_missing_key`:
    tests if missing mandatory configuration keys are correctly detected and reported
 -   `core/test_04-12_configuration_matrix`:
    tests if matrix values in configuration files are correctly parsed and interpreted
 -   `core/test_04-13_configuration_matrix_brackets`:
    tests if invalid or missing brackets of matrix values in configurations files are detected and reported
 -   `core/test_04-1_module_config_cli_change`:
    tests whether single configuration values can be overwritten by options supplied via the command line.
 -   `core/test_04-2_module_config_cli_nochange`:
    tests whether command line options are correctly assigned to module instances and do not alter other values.
 -   `core/test_04-3_configuration_imbalanced_brackets`:
    tests whether imbalanced brackets in configuration values are properly detected.
 -   `core/test_04-4_detector_config_cli_change`:
    tests whether detector options can be overwritten from the command line.
 -   `core/test_04-5_module_config_cli_detectors`:
    tests whether framework parameters are properly parsed from the command line.
 -   `core/test_04-6_module_config_double_unique`:
    tests whether a double definition of a unique module is detected.
 -   `core/test_04-7_module_config_empty_filter`:
    tests the framework behavior with an empty filter.
 -   `core/test_04-8_configuration_unused_key`:
    tests the detection of unused configuration keys in the global configuration section.
 -   `core/test_04-9_configuration_unused_key_module`:
    tests the detection of unused configuration keys in a module configuration section.
 -   `core/test_05-1_overwrite_same_denied`:
    tests whether two modules writing to the same file is disallowed if overwriting is denied.
 -   `core/test_05-2_overwrite_module_allowed`:
    tests whether two modules writing to the same file is allowed if the last one re-enables overwriting locally.
 -   `core/test_06-10_multithreading_physics_singlethr`:
    tests the reproducibility in case of multithreading disabled.
 -   `core/test_06-1_multithreading`:
    checks if multithreading can be enabled.
 -   `core/test_06-2_multithreading_cli`:
    checks if multithreading can be enabled from the command line.
 -   `core/test_06-3_multithreading_concurrency`:
    tests if the number of workers can be configured.
 -   `core/test_06-4_multithreading_zeroworkers`:
    tests the framework response in case too few workers are enabled.
 -   `core/test_06-5_multithreading_buffers`:
    tests if the module buffer depth can be configured properly.
 -   `core/test_06-6_multithreading_impossible`:
    tests the framework response in case a module without multithreading capabilities has been enabled.
 -   `core/test_06-7_multithreading_disabled`:
    tests the framework response to explicitly disabling multithreading.
 -   `core/test_06-8_multithreading_buffered`:
    tests the reproducibility in case of a sequential module.
 -   `core/test_06-9_multithreading_physics`:
    tests the reproducibility in case of multithreading enabled.
 -   `core/test_07-1_catch_exception`:
    checks the correct propagation of exceptions with multithreading enabled.
 -   `core/test_07-2_catch_exception_nomt`:
    checks the correct propagation of exceptions with multithreading disabled.
 -   `core/test_08-10_physics_recombination_srh`:
    tests selection of recombination model "srh"
 -   `core/test_08-11_physics_recombination_auger`:
    tests selection of recombination model "auger"
 -   `core/test_08-12_physics_recombination_combined`:
    tests selection of recombination model "combined"
 -   `core/test_08-13_physics_mobility_custom`:
    tests selection of a custom mobility model
 -   `core/test_08-1_physics_mobility_canali`:
    tests selection of mobility model "canali"
 -   `core/test_08-2_physics_mobility_hamburg`:
    tests selection of mobility model "hamburg"
 -   `core/test_08-3_physics_mobility_hamburg_highfield`:
    tests selection of mobility model "hamburg_highfield"
 -   `core/test_08-4_physics_mobility_masetti`:
    tests selection of mobility model "masetti"
 -   `core/test_08-5_physics_mobility_masetti_canali`:
    tests selection of mobility model "masetti_canali"
 -   `core/test_08-6_physics_mobility_arora`:
    tests selection of mobility model "arora"
 -   `core/test_08-7_physics_mobility_invalid`:
    tests if a selection of an invalid or non-existing mobility model is correctly detected and reported
 -   `core/test_08-8_physics_mobility_doping`:
    tests if the requirement of a doping profile for some mobility models is correctly detected and reported as error
 -   `core/test_08-9_physics_mobility_jacoboni`:
    tests selection of mobility model "jacoboni"
 -   `core/test_9-1_executable_version`:
    tests if the allpix executable correctly reports its version if requested
 -   `core/test_9-2_executable_help`:
    tests if the allpix executable correctly prints the help if requested
 -   `core/test_9-3_executable_loglevel_invalid`:
    tests if the allpix executable correctly reports invalid log levels set from the command line
 -   `core/test_9-4_executable_unrecognized_argument`:
    tests if the allpix executable correctly reports unrecognized command line arguments


## Module Functionality Tests

The following module functionality tests are currently performed:

-   `modules/CSADigitizer/01-pseudopulse`:
    checks the outcome of a digitization by the CSA of a pseudo-pulse generated from arrival times.
 -   `modules/CSADigitizer/02-tot_pseudopulse`:
    checks the outcome of a digitization by the CSA of a pseudo-pulse generated from arrival times and conversion to time-over-threshold units.
 -   `modules/CSADigitizer/03-custom`:
    tests initialization of a custom response function and its parameters
 -   `modules/CSADigitizer/04-custom_mupix`:
    tests the digitization with a custom response function
 -   `modules/CapacitiveTransfer/01-transfer`:
    tests the coupling of charge into neighbor pixels using a coupling matrix
 -   `modules/CorryvreckanWriter/01-corry`:
    ensures proper functionality of the Corryvreckan file writer module. The monitored output comprises the coordinates of the pixel produced in the simulation.
 -   `modules/CorryvreckanWriter/02-mc`:
    ensures the correct storage of Monte Carlo truth particle information in the Corryvreckan file writer module by monitoring the local coordinates of the MC particle associated to the pixel hit.
 -   `modules/DefaultDigitizer/01-charge`:
    digitizes the transferred charges to simulate the front-end electronics. The monitored output of this test comprises the total charge for one pixel including noise contributions and the smeared threshold it is compared to.
 -   `modules/DefaultDigitizer/02-qdc`:
    digitizes the transferred charges and tests the conversion into QDC units. The monitored output comprises the converted charge value in units of QDC counts.
 -   `modules/DefaultDigitizer/03-gain`:
    digitizes the transferred charges and tests the amplification process by monitoring the total charge after signal amplification and smearing.
 -   `modules/DefaultDigitizer/04-toa`:
    digitizes the signal and calculates the time-of-arrival of the particle by checking when the threshold was crossed.
 -   `modules/DefaultDigitizer/05-tdc`:
    digitizes the signal and test the conversion of time-of-arrival to TDC units.
 -   `modules/DefaultDigitizer/06-saturation`:
    tests the front-end saturation functionality
 -   `modules/DefaultDigitizer/07-gainfunction`:
    digitizes the transferred charges and tests the amplification process using a custom (surrogate) gain function.
 -   `modules/DefaultDigitizer/08-gain-gainfunction`:
    tests the correct detection of a simultaneous configuration of a default gain and a custom gain function.
 -   `modules/DefaultDigitizer/09-gainfunction-param`:
    tests the correct detection of an incorrect number of parameters provided for a custom gain function.
 -   `modules/DepositionCosmics/01-sealevel`:
    run basic simulation of cosmics shower
 -   `modules/DepositionCosmics/02-altitude`:
    check if the target simulation altitude can be configured
 -   `modules/DepositionCosmics/03-subbox`:
    test if the correct subbox letngth for the simulated shower is calculated from the detector model and world volume
 -   `modules/DepositionCosmics/04-noneutrons`:
    check if the emission of neutrons can be switched off effectively
 -   `modules/DepositionCosmics/05-latitude`:
    check if the latitude can be changed effectively
 -   `modules/DepositionCosmics/06-date`:
    check if the simulated date of the shower observation can be changed effectively
 -   `modules/DepositionCosmics/07-resettime`:
    test if the particle emission time can correctly be reset to zero when configured
 -   `modules/DepositionCosmics/08-maxparticles`:
    test if the maximum number of shower particles can be limited correctly
 -   `modules/DepositionGeant4/01-deposit`:
    executes the charge carrier deposition module. This will invoke Geant4 to deposit energy in the sensitive volume. The monitored output comprises the exact number of charge carriers deposited in the detector.
 -   `modules/DepositionGeant4/02-mc`:
    executes the charge carrier deposition module as the previous tests, but monitors the type, entry and exit point of the Monte Carlo particle associated to the deposited charge carriers.
 -   `modules/DepositionGeant4/03-track`:
    executes the charge carrier deposition module as the previous tests, but monitors the start and end point of one of the Monte Carlo tracks in the event.
 -   `modules/DepositionGeant4/04-source_point`:
    tests the point source in the charge carrier deposition module by monitoring the deposited charges.
 -   `modules/DepositionGeant4/05-source_square`:
    tests the square source in the charge carrier deposition module by monitoring the deposited charges.
 -   `modules/DepositionGeant4/06-source_sphere`:
    tests the sphere source in the charge carrier deposition module by monitoring the deposited charges.
 -   `modules/DepositionGeant4/07-source_macro`:
    tests the G4 macro source in the charge carrier deposition module using the macro file `source_macro_test.txt`, monitoring the deposited charges.
 -   `modules/DepositionGeant4/08-fano`:
    tests the simulation of fluctuations in charge carrier generation by monitoring the total number of generated carrier pairs when altering the Fano factor.
 -   `modules/DepositionGeant4/09-ions`:
    tests if custom ions can be fored to decay immediately
 -   `modules/DepositionGeant4/10-all_tracks`:
    runs a single Geant4 event and retrieves all tracks from the event, including those without connection to the sensor volume.
 -   `modules/DepositionPointCharge/01-point`:
    tests the deposition of a point charge at a specified position, checks the position of the deposited charge carrier in global coordinates.
 -   `modules/DepositionPointCharge/02-scan`:
    tests the scan of a pixel volume by depositing charges for a given number of events, check for the calculated voxel size.
 -   `modules/DepositionPointCharge/03-scan_cube`:
    tests the calculation of the scanning points by monitoring the warning of the number of events is not a perfect cube.
 -   `modules/DepositionPointCharge/04-mip`:
    tests the deposition of charges along a line by monitoring the calculated step size and number of charge carriers deposited per step.
 -   `modules/DepositionPointCharge/05-mip_position`:
    tests the generation of the Monte Carlo particle when depositing charges along a line by monitoring the start and end positions of the particle.
 -   `modules/DepositionPointCharge/05-spot`:
    tests the deposition of charge carriers around a fixed position with a Gaussian distribution.
 -   `modules/DepositionReader/01-csv`:
    tests reading in a CSV file generated according to the specifications
 -   `modules/DepositionReader/02-root`:
    tests reading in a ROOT file generated according to the specifications
 -   `modules/DepositionReader/03-no_time_csv`:
    tests reading in a CSV file generated according to the specifications and without timing information
 -   `modules/DepositionReader/04-no_time_root`:
    tests reading in a ROOT file generated according to the specifications and without timing information
 -   `modules/DepositionReader/05-no_mcp_csv`:
    tests reading in a CSV file generated according to the specifications and without Monte Carlo particle information
 -   `modules/DepositionReader/06-no_mcp_root`:
    tests reading in a ROOT file generated according to the specifications and without Monte Carlo particle information
 -   `modules/DepositionReader/07-root_notree`:
    tests if a missing ROOT tree is detected correctly
 -   `modules/DepositionReader/08-root_branches`:
    tests if the number of branch names configured is correctly calculated
 -   `modules/DepositionReader/09-root_branch_wrong`:
    tests if missing or wrongly named branches are properly detected and reported
 -   `modules/DepositionReader/10-wrong_detector`:
    tests if depositions from a detector not present in the current simulation are ignored correctly
 -   `modules/DepositionReader/11-outside_sensor`:
    tests if deposited energies outside the active sensor volume of the detector are ignored correctly
 -   `modules/DepositionReader/12-end_of_file`:
    tests if a premature end of the CSV input file is correctly reported and the simulation terminated properly
 -   `modules/DepositionReader/13-end_of_tree`:
    tests if a premature end of the input ROOT tree is correctly reported and the simulation terminated pr
 -   `modules/DepositionReader/14-truncate_csv`:
    tests if detector name truncation works as expected in CSV files
 -   `modules/DepositionReader/15-truncate_root`:
    tests if detector name truncation works as expected in ROOT trees
 -   `modules/DepositionReader/16-mcp_ordered`:
    tests if parent relations of Monte Carlo particles are correctly determined and recorded
 -   `modules/DepositionReader/17-mcp_unordered`:
    tests if parent relations of Monte Carlo particles are correctly determined and recorded even if they appear unordered in the input data
 -   `modules/DetectorHistogrammer/01-histogramming`:
    tests the detector histogramming module and its clustering algorithm. The monitored output comprises the total number of clusters.
 -   `modules/DopingProfileReader/01-regions`:
    tests if a doping profile can be configured by means of different concentration regions in depth
 -   `modules/DopingProfileReader/02-constant`:
    tests if a constant doping profile can be configured
 -   `modules/ElectricFieldReader/01-linear`:
    creates a linear electric field in the constructed detector by specifying the bias and depletion voltages. The monitored output comprises the calculated effective thickness of the depleted detector volume.
 -   `modules/ElectricFieldReader/02-mesh`:
    loads an INIT file containing a TCAD-simulated electric field and applies the field to the detector model. The monitored output comprises the number of field cells for each pixel as read and parsed from the input file.
 -   `modules/ElectricFieldReader/03-linear_depth`:
    creates a linear electric field in the constructed detector by specifying the applied bias voltage and a depletion depth. The monitored output comprises the calculated effective thickness of the depleted detector volume.
 -   `modules/ElectricFieldReader/04-linear_depletion_side`:
    checks that depleting from the sensor backside is possible.
 -   `modules/ElectricFieldReader/05-constant`:
    tests the possibility of setting a constant electric field
 -   `modules/ElectricFieldReader/06-mutually_exclusive`:
    tests that the mutually exclusive parameters depletion\_depth and depletion\_voltage cannot be used together
 -   `modules/ElectricFieldReader/07-sensor_thickness`:
    tests that the depltion thickness cannot be larger than the sensor thickness
 -   `modules/ElectricFieldReader/08-mesh_offset`:
    tests the possibility of configuring an offset for the mesh
 -   `modules/ElectricFieldReader/09-mesh_offset_negative`:
    tests that the mesh offset cannot be negative
 -   `modules/ElectricFieldReader/10-mesh_offset_large`:
    tests that the mesh offset cannot be larger than one pixel pitch
 -   `modules/ElectricFieldReader/11-mesh_scale`:
    tests the possibility to scale the mesh in x and y
 -   `modules/ElectricFieldReader/12-parabolic`:
    tests the parabolic electric field
 -   `modules/ElectricFieldReader/13-parabolic_minimum_pos`:
    tests if the minimum position is required to be within the defined electric field region
 -   `modules/ElectricFieldReader/14-custom_1d`:
    tests the possibility of setting a one-dimensional custom electric field function
 -   `modules/ElectricFieldReader/15-custom_3d`:
    tests the possibility of setting a three-dimensional custom electric field function
 -   `modules/ElectricFieldReader/16-custom_functions`:
    tests that the custom function either requires one or three components
 -   `modules/ElectricFieldReader/17-custom_parameters_1d`:
    tests that the number of parameters provided to custom one-dimensional field functions needs to match
 -   `modules/ElectricFieldReader/18-custom_parameters_3d`:
    tests that the number of parameters provided to custom three-dimensional field functions needs to match
 -   `modules/ElectricFieldReader/19-linear-largefield`:
    tests if very high bias voltages are correctly detected and reported as warning
 -   `modules/GenericPropagation/01-propagation`:
    uses the Runge-Kutta-Fehlberg integration of the equations of motion implemented in the drift-diffusion model to propagate the charge carriers to the implants. The monitored output comprises the total number of charges moved, the number of integration steps taken and the simulated propagation time.
 -   `modules/GenericPropagation/02-magnetic`:
    uses the Runge-Kutta-Fehlberg integration of the equations of motion implemented in the drift-diffusion model to propagate the charge carriers to the implants under the influence of a constant magnetic field. The monitored output comprises the total number of charges moved, the number of integration steps taken and the simulated propagation time.
 -   `modules/GenericPropagation/03-lifetime`:
    test recombination of charge carriers during drift
 -   `modules/GenericPropagation/04-mobility_unsuitable`:
    tests if the selection of doping-dependent mobility models without doping information is caught correctly
 -   `modules/GenericPropagation/05-mobility_nomodel`:
    tests if non-existing mobility models selected in the configuration file are detected
 -   `modules/GenericPropagation/06-no_lifetime`:
    tests the fallback of infinite charge carrier lifetime in case no recombination model is chosen
 -   `modules/GenericPropagation/07-lifetime_unsuitable`:
    tests if the selection of doping-dependent recombination models without doping information is caught correctly
 -   `modules/GenericPropagation/08-lifetime_nomodel`:
    tests if non-existing recombination models selected in the configuration file are detected
 -   `modules/GenericPropagation/09-no_trapping`:
    tests the fallback of infinite charge carrier lifetime in case no trapping model is chosen
 -   `modules/GenericPropagation/10-trapping_nomodel`:
    tests if non-existing trapping models selected in the configuration file are detected
 -   `modules/GenericPropagation/11-trapping_custom`:
    tests functionality of custom trapping model
 -   `modules/GeometryBuilderGeant4/01-build`:
    takes the provided detector setup and builds the Geant4 geometry from the internal detector description. The monitored output comprises the calculated wrapper dimensions of the detector model.
 -   `modules/GeometryBuilderGeant4/02-addpoint`:
    ensures the module adds corner points of the passive material in a correct way.
 -   `modules/GeometryBuilderGeant4/03-addpoint_rotate`:
    ensures proper rotation of the position of the corner points of the passive material.
 -   `modules/GeometryBuilderGeant4/04-mothervolume`:
    ensures placing a detector inside a passive material will not cause overlapping materials.
 -   `modules/GeometryBuilderGeant4/05-worldvolume`:
    ensures the added corner points of the passive material increase the world volume accordingly.
 -   `modules/GeometryBuilderGeant4/06-same_materials`:
    tests if a warning will be thrown if the material of the passive material is the same as the material of the world volume.
 -   `modules/GeometryBuilderGeant4/07-radial_build`:
    builds the Geant4 geometry of a radial strip detector. The monitored output is the world size based on the wrapper dimensions and rotation
 -   `modules/LCIOWriter/01-lcio`:
    ensures proper functionality of the LCIO file writer module. Similar to the above test, the correct conversion of PixelHits (coordinates and charge) is monitored.
 -   `modules/LCIOWriter/02-detector_assignment`:
    exercises the assignment of detector IDs to Allpix Squared detectors in the LCIO output file. A fixed ID and collection name is assigned to the simulated detector.
 -   `modules/LCIOWriter/03-no_mc_truth`:
    ensures that simulation results are properly converted to LCIO and stored even without the Monte Carlo truth information available.
 -   `modules/MagneticFieldReader/01-constant`:
    creates a constant magnetic field for the full volume and applies it to the geometryManager. The monitored output comprises the message for successful application of the magnetic field.
 -   `modules/MagneticFieldReader/02-local`:
    checks that the local magnetic field including the detector rotation is correct.
 -   `modules/ProjectionPropagation/01-project`:
    projects deposited charges to the implant side of the sensor. The monitored output comprises the total number of charge carriers propagated to the sensor implants.
 -   `modules/ProjectionPropagation/02-lifetime`:
    projects deposited charges to the implant side of the sensor with a reduced integration time to ignore some charge carriers. The monitored output comprises the total number of charge carriers propagated to the sensor implants.
 -   `modules/PulseTransfer/01-pseudopulse`:
    tests the calculation of induced signals based on the arrival time of charge carriers at the sensor surface.
 -   `modules/PulseTransfer/02-pseudopulse-ancestors`:
    tests the calculation of induced signals based on the arrival time of charge carriers at the sensor surface.
 -   `modules/RCEWriter/02-write`:
    ensures proper functionality of the RCE file writer module. The correct conversion of the PixelHit position and value is monitored by the test's regular expressions.
 -   `modules/ROOTObjectReader/01-reading`:
    tests the capability of the framework to read data back in and to dispatch messages for all objects found in the input tree. The monitored output comprises the total number of objects read from all branches.
 -   `modules/ROOTObjectReader/02-seed_mismatch`:
    tests the capability of the framework to detect different random seeds for misalignment set in a data file to be read back in. The monitored output comprises the error message including the two different random seed values.
 -   `modules/ROOTObjectReader/03-seed_ignore`:
    tests if core random seeds are properly ignored by the ROOTObjectReader module if requested by the configuration. The monitored output comprises the warning message emitted if a difference in seed values is discovered.
 -   `modules/ROOTObjectWriter/01-write`:
    ensures proper functionality of the ROOT file writer module. It monitors the total number of objects and branches written to the output ROOT trees.
 -   `modules/SimpleTransfer/01-transfer`:
    tests the transfer of charges from sensor implants to readout chip. The monitored output comprises the total number of charges transferred and the coordinates of the pixels the charges have been assigned to.
 -   `modules/SimpleTransfer/02-implant`:
    tests the transfer of charges from sensor implants to readout chip in case sensor implants have been defined in the detector model.
 -   `modules/SimpleTransfer/03-radial_transfer`:
    tests the transfer of charges from sensor implants to readout chip in a radial strip detector. The monitored output comprises the total number of charges transferred and the coordinates of the pixels the charges have been assigned to.
 -   `modules/TextWriter/01-write`:
    ensures proper functionality of the ASCII text writer module by monitoring the total number of objects and messages written to the text file.
 -   `modules/WeightingPotentialReader/01-pad`:
    tests the on-the-fly generation of the weighting potential following the plane condenser with "pad over infinite plane" approach.


## Performance Tests

Current performance tests comprise:

-   `performance/test_01_deposition`:
    tests the performance of charge carrier deposition in the sensitive sensor volume using Geant4. A stepping length of 1.0 um is chosen, and 10000 events are simulated. The addition of an electric field and the subsequent projection of the charges are necessary since Allpix Squared would otherwise detect that there are no recipients for the deposited charge carriers and skip the deposition entirely.
 -   `performance/test_02-1_propagation_generic`:
    tests the very critical performance of the drift-diffusion propagation of charge carriers, as this is the most computing-intense module of the framework. Charge carriers are deposited and a propagation with 10 charge carriers per step and a fine spatial and temporal resolution is performed. The simulation comprises 500 events.
 -   `performance/test_02-2_propagation_project`:
    tests the projection of charge carriers onto the implants, taking into account the diffusion only. Since this module is less computing-intense, a total of 5000 events are simulated, and charge carriers are propagated one-by-one.
 -   `performance/test_02-3_propagation_generic_multithread`:
    tests the performance of multithreaded simulation. It utilizes the very same configuration as performance test 02-1 but in addition enables multithreading with four worker threads.
 -   `performance/test_03_multithreading`:
    tests the performance of the framework when using multithreading with 4 workers to simulate 500 events. It uses a similar configuration as the example configuration.

