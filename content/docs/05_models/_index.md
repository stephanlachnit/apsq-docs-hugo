---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Physics Models & Material Properties"
description: "Description of the different physics models for semiconductor detectors."
weight: 5
---

Allpix Squared implements a variety of properties and models to describe the physics of semiconductor detectors. Models are
implemented module-independently and can be selected via configuration parameters in the respective models, while sensor
material properties serve as a default to module parameters and can be overwritten in the respective configuration section.
This chapter serves as central reference for the different properties and models.
