---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Contributing Code"
weight: 3
---

Since Allpix Squared is a community project that benefits from active participation in the development and code contributions
from users. Users and prospective developers are encouraged to discuss their needs either via the issue tracker of the
repository \[[@ap2-issue-tracker]\], the forum \[[@ap2-forum]\] or the developer's mailing list to receive ideas and guidance
on how to implement a specific feature. Getting in touch with other developers early in the development cycle avoids spending
time on features which already exist or are currently under development by other users.

The repository contains a few tools to facilitate contributions and to ensure code quality as detailed in
[Chapter 10](../../10_devtools).


[@ap2-issue-tracker]: https://gitlab.cern.ch/allpix-squared/allpix-squared/issues
[@ap2-forum]: https://cern.ch/allpix-squared-forum
