---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Objects"
description: "Objects which can be used to transfer data between modules."
weight: 6
---

Allpix Squared provides a set of objects which can be used to transfer data between modules. These objects can be sent with
the messaging system as explained in [Section 4.6](../04_framework/06_messages).
