---
title: "Publications"
layout: docs
---

{{< blocks/cover title="Publications" height="auto" >}}
{{< /blocks/cover >}}

{{% blocks/section type="section" %}}
## Reference Publications

S. Spannagel et al., [**Allpix Squared: A modular simulation framework for silicon detectors**](https://doi.org/10.1016/j.nima.2018.06.020),  
Nucl. Instr. Meth. A 901 (2018) 164 – 172, [doi:10.1016/j.nima.2018.06.020](https://doi.org/10.1016/j.nima.2018.06.020), [arXiv:1806.05813](https://arxiv.org/abs/1806.05813)

D. Dannheim et al., [**Combining TCAD and Monte Carlo methods to simulate CMOS pixel sensors with a small collection electrode using the Allpix Squared framework**](https://doi.org/10.1016/j.nima.2020.163784),  
Nucl. Instr. Meth. A 964 (2020), [doi:10.1016/j.nima.2018.06.020](https://doi.org/10.1016/j.nima.2020.163784), [arXiv:1806.05813](https://arxiv.org/abs/2002.12602)
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
## Workshops & Tutorials

P. Schütze, S. Spannagel, [**Hands-On: Silicon Detector Monte-Carlo Simulations with Allpix Squared - Beginners**](https://indico.cern.ch/event/945675/contributions/4181048/),  
9th Beam Telescopes and Test Beams Workshop,  
*08-10 February 2021, Virtual*

P. Schütze, S. Spannagel, [**Hands-On: Everything you would like to know about Allpix Squared - Advanced**](https://indico.cern.ch/event/945675/contributions/4181049/),  
9th Beam Telescopes and Test Beams Workshop,  
*08-10 February 2021, Virtual*

D. Hynds, S. Spannagel, [**Hands-On: Silicon Detector Monte-Carlo Simulations with Allpix Squared**](https://indico.cern.ch/event/813822/contributions/3648304/),  
8th Beam Telescopes and Test Beams Workshop,  
*27-31 January 2020, Tbilisi, Georgia*

S. Spannagel, [**Hands-On: The Allpix Squared Simulation Framework**](https://indico.cern.ch/event/731649/contributions/3253672/),  
7th Beam Telescopes and Test Beams Workshop,  
*14-18 January 2019, CERN, Switzerland*

[**1st Allpix Squared User Workshop**](https://indico.cern.ch/event/738283/),  
*26-27 November 2018, CERN, Switzerland*

S. Spannagel, [**Hands-On: The Allpix Squared Simulation Framework**](https://indico.desy.de/indico/event/18050/session/3/contribution/65),  
6th Beam Telescopes and Test Beams Workshop,  
*16-19 January 2018, Zurich, Switzerland*
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
## Presentations

S. Spannagel, [**What's new on Allpix Squared?**](https://indico.cern.ch/event/945675/contributions/4159534/),  
9th Beam Telescopes and Test Beams Workshop,  
*08-10 February 2021, Virtual*

P. Schütze, [**Status and Plans for Allpix Squared**](https://indico.cern.ch/event/984049/),  
CERN EP R&D WP1.4 Meeting: Monolithic Sensor Simulations,  
*16 December 2020, Virtual*

S. Spannagel, [**The Allpix Squared Framework: New Developments**](https://indico.cern.ch/event/958135/contributions/4030964/),  
CLIC Detector and Physics Collaboration,  
*01 October 2020, Virtual*

S. Spannagel, [**The Importance of Insight: Monte Carlo Simulations in Silicon Pixel Detector Development**](https://instrumentationseminar.desy.de/sites2009/site_instrumentationseminar/content/e70397/e282395/e289794/2019-08-09-DESY-Instrumentation-Seminar_MC-Si-Detectors.pdf),  
DESY Joint Instrumentation Seminar,  
*09 August 2019, DESY, Hamburg, Germany*

S. Spannagel, [**Monte Carlo Simulations for Silicon Detectors: Bridging the Gap between Detector Design and Prototype Testing**](https://indico.cern.ch/event/811852/),  
CERN Detector Seminar,  
*12 April 2019, CERN, Switzerland*

S. Spannagel, [**The Allpix Squared Framework**](https://indico.cern.ch/event/804477/),  
EP-SFT Group Meeting,  
*8 April 2019, CERN, Switzerland*

S. Spannagel, [**Status and Plans for Allpix Squared**](https://indico.cern.ch/event/753671/contributions/3278131/),  
CLIC Workshop,  
*21-25 January 2019, CERN, Switzerland*

S. Spannagel, [**Updates on Allpix Squared**](https://indico.cern.ch/event/731649/contributions/3237196/),  
7th Beam Telescopes and Test Beams Workshop,  
*14-18 January 2019, CERN, Switzerland*

T. Billoud, **New features of the Allpix Squared simulation framework**,  
Medipix Collaboration Meeting,  
*19 September 2018, CERN, Switzerland*

S. Spannagel, [**Combining TCAD and Monte Carlo Methods to Simulate High-Resistivity CMOS Pixel Detectors using the Allpix Squared Framework**](https://indico.cern.ch/event/703821/contributions/3107881/),  
CLIC Detector and Physics Collaboration Meeting,  
*28-29 August, CERN, Switzerland*

P. Schütze, [**Allpix Squared - Recent Developments**](https://indico.cern.ch/event/703821/contributions/3107861/),  
CLIC Detector and Physics Collaboration Meeting,  
*28-29 August, CERN, Switzerland*

V. Sonesten, [**Event-based Multi-Threading in Allpix Squared**](https://indico.cern.ch/event/748000/),  
*9 August 2018, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.cern.ch/event/656356/contributions/2848670/),  
CLIC Workshop,  
*22-26 January 2018, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.desy.de/indico/event/18050/session/10/contribution/0),  
6th Beam Telescopes and Test Beams Workshop,  
*16-19 January 2018, Zurich, Switzerland*

D. Hynds, **Allpix Squared - a Generic Pixel Detector Simulation Framework**,  
Medipix Collaboration Meeting,  
*29 November 2017, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.cern.ch/event/663851/contributions/2788161/),  
31st RD50 Workshop,  
*20-22 November 2017, CERN, Switzerland*

K. Wolters, [**The Allpix2 Simulation Framework**](https://indico.cern.ch/event/633975/contributions/2686446/),  
CLIC Detector and Physics Collaboration Meeting,  
*29-30 August 2017, CERN, Switzerland*
{{% /blocks/section %}}
