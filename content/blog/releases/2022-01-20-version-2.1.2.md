---
title: "Patch Release 2.1.2"
date: 2022-01-20T14:16:32+01:00
draft: false
---

We would like to announce the second patch release for the 2.1 series of **{{% allpix %}}, version 2.1.2**.
This release contains 21 commits over the last patch version 2.1.1.
The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->

* **Core**
   * The `MCParticle` object now carries a method `isPrimary()` to check whether it is a primary particle or not.
   * The interface to Geant4's logging output has been improved by first setting up logging destinations and then initializing the geometry. Also, an exception handler is now registered with Geant4, allowing {{% allpix %}} to catch them and to correctly end the simulation with our own `ModuleError`.
* **Manual:** Some additional context has been added to the Auger recombination model.
* **Continuous Integration:** The check for a working compiler version has been fixed: we require a GCC version of at least 9.0 owing to the `std::filesystem` features as well as the 3rdparty dependency "MagicEnum" we use for `enum` reflection. This was the case already before, but now CMake correctly checks for it. The target C++ version C++17 is now set as `clang-format` standard.
* **Module DatabaseWriter:** The dependency `libpqxx`, a C++ interface library for PostgreSQL databases, is now looked up using the `PkgConfig` tool. Since it is available also on CVMFS, this module is now also built on all CVMFS builds of the CI pipeline as well as the Docker images.
